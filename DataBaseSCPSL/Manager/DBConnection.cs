﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Smod2;

namespace DataBaseSCPSL.Manager
{
    public class DBConnection
    {
        public MySqlConnection connection;
        public string ipaddress = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_ipaddress", "127.0.0.1").Trim(' ');
        public string username = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_username", "root").Trim(' ');
        public string password = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_password", string.Empty).Trim(' ');
        public string database = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_database", "scpsl").Trim(' ');
        public string port = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_port", "3306").Trim(' ');

        public string table = ConfigManager.Manager.Config.GetStringValue("dbsql_sql_table", "stats").Trim(' ');

        public Plugin plugin; 

        public DBConnection(Plugin plugin)
        {
            this.InitConnection();
            this.plugin = plugin;
        }

        public void InitConnection()
        {  
            string connectionString = $"SERVER={ipaddress};DATABASE={database};UID={username};PASSWORD={password};PORT={port}";
            this.connection = new MySqlConnection(connectionString);
        }

        public void AddTable(string tablename)
        {
                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"CREATE TABLE IF NOT EXISTS `{table}` (`steamid64` VARCHAR(17) NOT NULL,`firstconnexion` DATETIME NOT NULL,`username` VARCHAR(32) NULL DEFAULT NULL,`kills` INT(11) NOT NULL DEFAULT '0',`teamkills` INT(11) NOT NULL DEFAULT '0',`deaths` INT(11) NOT NULL DEFAULT '0',`rank` VARCHAR(50) NOT NULL DEFAULT 'user',`escape` INT(11) NOT NULL DEFAULT '0',`class_scp173` INT(11) NOT NULL DEFAULT '0',`class_classd` INT(11) NOT NULL DEFAULT '0',`class_scp106` INT(11) NOT NULL DEFAULT '0',`class_ntf_scientist` INT(11) NOT NULL DEFAULT '0',`class_scp049` INT(11) NOT NULL DEFAULT '0',`class_scientist` INT(11) NOT NULL DEFAULT '0',`class_scp079` INT(11) NOT NULL DEFAULT '0',`class_ci` INT(11) NOT NULL DEFAULT '0',`class_scp096` INT(11) NOT NULL DEFAULT '0',`class_scp0492` INT(11) NOT NULL DEFAULT '0',`class_ntfltn` INT(11) NOT NULL DEFAULT '0',`class_ntfcmd` INT(11) NOT NULL DEFAULT '0',`class_ntf_cd` INT(11) NOT NULL DEFAULT '0',`class_tut` INT(11) NOT NULL DEFAULT '0',`class_fguard` INT(11) NOT NULL DEFAULT '0',`class_scp939` INT(11) NOT NULL DEFAULT '0',`last_connexion` DATETIME NULL DEFAULT NULL,`gameplayed` INT(11) NOT NULL DEFAULT '0',PRIMARY KEY (`steamid64`))COLLATE='latin1_swedish_ci'ENGINE=InnoDB;";
                command.ExecuteNonQuery();

                connection.Close();
        }

        public void AddPlayer(string steamid64, string username)
        {
            try
            {
                connection.Open();

                MySqlCommand cmdwarn = connection.CreateCommand();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT IGNORE INTO {table} (steamid64, username, firstconnexion) VALUES (@steamid64, @username, @firstconnexion)";

                command.Parameters.AddWithValue("@steamid64", steamid64.TrimStart(' ').TrimEnd(' '));
                command.Parameters.AddWithValue("@username", username.TrimStart(' ').TrimEnd(' '));
                command.Parameters.AddWithValue("@firstconnexion", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                command.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
            }
            finally
            {
                connection.Close();
            }
        }

        public void UpdateValue(string steamid64, string column, object value, string tablename)
        {
            try
            {
                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                if(value is string && (string)value == "NULL")
                {
                    command.CommandText = $"UPDATE {tablename} SET {column} = NULL WHERE steamid64 = '{steamid64}'";
                }
                else
                {
                    command.CommandText = $"UPDATE {tablename} SET {column} = '{value}' WHERE steamid64 = '{steamid64}'";
                }
                command.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
            }
            finally
            {
                connection.Close();
            }
        }

        public void IncrementINT(string column, string steamid64, string table, int value)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();

                command.CommandText = $"UPDATE {table} SET {column}={column}+{value} WHERE steamid64 = '{steamid64}'";
                command.ExecuteNonQuery();

                connection.Close();
            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
            }
            finally
            {
                connection.Close();
            }
        }

        public string GetString(string column, string steamid64, string table)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT {column} FROM {table} WHERE steamid64 = '{steamid64}'";
                var dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        string value = dr.GetString(column);
                        dr.Close();
                        connection.Close();
                        return value;
                    }
                }
                return "";

            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
                return "";
            }
            catch (System.Data.SqlTypes.SqlNullValueException)
            {
                return "";
            }
            finally
            {
                connection.Close();
            }
        }

        public DateTime GetDateTime(string column, string steamid64, string table)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT {column} FROM {table} WHERE steamid64 = '{steamid64}'";
                var dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        DateTime value = dr.GetDateTime(column);
                        dr.Close();
                        connection.Close();
                        return value;
                    }
                }
                return DateTime.ParseExact("2000-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
                return DateTime.ParseExact("2000-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch(System.Data.SqlTypes.SqlNullValueException)
            {
                return DateTime.ParseExact("2000-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            finally
            {
                connection.Close();
            }
        }

        public int GetInt(string column, string steamid64, string table)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT {column} FROM {table} WHERE steamid64 = '{steamid64}'";
                var dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        int value = dr.GetInt32(column);
                        dr.Close();
                        connection.Close();
                        return value;
                    }
                }
                return 0;

            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
                return 0;
            }
            catch (System.Data.SqlTypes.SqlNullValueException)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool GetBoolean(string column, string steamid64, string table)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT {column} FROM {table} WHERE steamid64 = '{steamid64}'";
                var dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        bool value = dr.GetBoolean(column);
                        dr.Close();
                        connection.Close();
                        return value;
                    }
                }
                return false;

            }
            catch (MySqlException e)
            {
                plugin.Error($"@#fg=Red;{ e.ErrorCode } { e.Message } ({ e.Source}).");
                return false;
            }
            catch (System.Data.SqlTypes.SqlNullValueException)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }
        }

        public string ConvertDateTime(DateTime datetime)
        {
            return datetime.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }

    public class DBPlayer
    {

        public string steamid64;
        public string username;

        public int kills;
        public int teamkills;
        public int deaths;

        public int class_scp173;
        public int class_classd;
        public int class_scp106;
        public int class_ntf_scientist;
        public int class_scp049;
        public int class_scientist;
        public int class_scp079;
        public int class_ci;
        public int class_scp096;
        public int class_scp0492;
        public int class_ntlftn;
        public int class_ntfcmd;
        public int class_ntf_cd;
        public int class_tut;
        public int class_fguard;
        public int class_scp939;



        public DateTime firstconnexion;
        public DateTime lastconnexion;
        public DateTime unban_time;
        public string ban_reason;
    }
}
