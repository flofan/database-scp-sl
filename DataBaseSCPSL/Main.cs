﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseSCPSL.Manager;
using Smod2;
using Smod2.Attributes;

namespace DataBaseSCPSL
{
    [PluginDetails(author = "Flo - Fan", description = "Integrate a database for SCP SL", id = "flo.dbsl", name = "DataBase Integration", SmodMajor = 3, SmodMinor = 4, SmodRevision = 0, version = "0.0.1")]

    public class Main : Plugin
    {
        public override void OnDisable()
        {
            this.Info("Plugin @#fg=Red;Disabled");
        }

        public override void OnEnable()
        {

            DBConnection db = new DBConnection(this);
            this.Info("Plugin @#fg=Green;Enabled");
        }

        public override void Register()
        {

            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_ipaddress", "127.0.0.1", true, "Enter the IP Address of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_username", "root", true, "Enter the username of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_password", string.Empty, true, "Enter the password of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_database", "scpsl", true, "Enter the name of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_port", "3306", true, "Enter the name of the database"));
            AddConfig(new Smod2.Config.ConfigSetting("dbsl_sql_table", "stats", true, "Enter the name of the table where the data will be stored"));


            AddEventHandlers(new Handler.EventHandlers(this));
            AddCommands(new string[] { "sqlinfo" }, new Command.PlayerInfo(this));
        }

    }
}
